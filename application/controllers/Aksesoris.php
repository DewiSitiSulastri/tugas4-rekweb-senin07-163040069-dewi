<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aksesoris extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/rest_server2/";
	}

	public function index()
	{
		$data['judul'] = 'Latihan Rest API';
		$data['aksesoris'] = json_decode($this->curl->simple_get($this->API . '/aksesoris'));
		$data['aks'] = $data['aksesoris'];
		$data['content'] = 'aksesoris/aksesoris';

		$this->load->view('template/template', $data);
	}

	public function detail($id)
	{
		$data['judul'] = 'Latihan Rest API';
		$data['aksesoris'] = json_decode($this->curl->simple_get($this->API . '/aksesoris/', array("id" => $id)));
		$data['content'] = 'aksesoris/detail';

		$this->load->view('template/template', $data);
	}

	public function search()
	{
		$search = urldecode($this->input->get("keyword", true));
		$data['judul'] = 'Latihan Rest API';
		$data['aksesoris'] = json_decode($this->curl->simple_get($this->API . '/aksesoris/search/?cari='. urldecode($search)));
		$data['aks'] = $data['aksesoris'];
		$data['content'] = 'aksesoris/aksesoris';

		$this->load->view('template/template', $data);
	}

	public function create()
	{
		
		$nama = $this->input->post('nama');
		$jenis = $this->input->post('jenis');
		$stok = $this->input->post('stok');
		$harga = $this->input->post('harga');
		$gambar = $this->input->post('gambar');

		$data = array(
				'nama' => $nama,
				'jenis' => $jenis,
				'stok' => $stok,
				'harga' => $harga,
				'gambar' => $gambar,
				);
		$this->curl->simple_post($this->API . '/aksesoris/', $data, array(CURLOPT_BUFFERSIZE => 10));
		echo json_encode(array("status" => true ));

		

		return "Data Berhasil Masuk";

		
	}

	public function edit()
	{
		$id = $this ->input->post("id");
		$params = array('id' => $id);
		$data['aksesoris'] = json_encode($this->curl->simple_get($this->API . '/aksesoris/', $params ));
		$aksesoris = $data['aksesoris'];
		$json = json_encode(array("status" => 200, "aks" => $aksesoris));
		echo $json;
	}

	public function update()
	{
		$nama = $this->input->post('namaUpdate');
		$jenis = $this->input->post('jenisUpdate');
		$stok = $this->input->post('stokUpdate');
		$harga = $this->input->post('hargaUpdate');
		$gambar = $this->input->post('gambarUpdate');
		$id = $this->input->post("id");

		$data = array(
				'id' => $id,
				'nama' => $nama,
				'jenis' => $jenis,
				'stok' => $stok,
				'harga' => $harga,
				'gambar' => $gambar,
				);
		$this->curl->simple_put($this->API . '/aksesoris/', $data, array(CURLOPT_BUFFERSIZE => 10));
		echo json_encode(array("status" => TRUE ));
	}

	public function delete()
	{
		$id = $this->input->post("id");
		json_decode($this->curl->simple_delete($this->API . '/aksesoris/', array('id' => $id), array(CURLOPT_BUFFERSIZE => 10)));
		echo json_encode(array("status" => TRUE ));
	}
}
