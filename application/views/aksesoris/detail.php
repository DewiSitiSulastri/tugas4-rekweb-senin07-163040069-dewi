<div class="container mt-5">
	<div class="card" style="width: 18rem;">
		<div class="card-body">
			<h5 class="card-title"><?= $aksesoris[0]->nama ?></h5>
			<h6 class="card-subtitle mb-2 text-muted"><?= $aksesoris[0]->jenis ?></h6>
			<p class="card-text"><?= $aksesoris[0]->stok ?></p>
			<p class="card-text"><?= $aksesoris[0]->harga ?></p>
			<a href="<?= site_url('aksesoris') ?>" class="card-link">Kembali</a>
		</div>
	</div>
</div>